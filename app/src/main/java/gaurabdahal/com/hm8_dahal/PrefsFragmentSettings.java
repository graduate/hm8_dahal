/*
Gaurab Dahal (L20432055)
 */

package gaurabdahal.com.hm8_dahal;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.util.Log;

/**
 * Created by gaurabdahal on 7/11/17.
 */
public class PrefsFragmentSettings  extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener{

    public PrefsFragmentSettings(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.prefs_fragment_settings);
    }

    public void onResume(){
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);

        Preference prefs;
        prefs = getPreferenceScreen().findPreference("key_zoo_animals");
        prefs.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                try {
                    Uri site = Uri.parse("https://nationalzoo.si.edu/animals/list");
                    Intent intent = new Intent(Intent.ACTION_VIEW, site);
                    startActivity(intent);
                } catch (Exception e) {
                    Log.e("PrefsFragmentSetting", "Browser Failed", e);
                }
                return true;
            }
        });

        if( Assets.mp != null){
            Assets.mp.start();
        }
    }

    public void onPause(){
        super.onPause();
        if(Assets.mp != null) {
            Assets.mp.pause();
        }
    }

    /**
     * Called when a shared preference is changed, added, or removed. This
     * may be called even if a preference is set to its existing value.
     */
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if(key.equals("key_music_enabled")){
            boolean b = sharedPreferences.getBoolean("key_music_enabled",true);
            if(b==false){
                if(Assets.mp != null){
                    Assets.mp.setVolume(0,0);
                }
            }else{
                if(Assets.mp != null){
                    Assets.mp.setVolume(1,1);
                }
            }
        }
        else if(key.equals("key_soundeffect_enabled")){
                Assets.soundpool_active=sharedPreferences.getBoolean("key_soundeffect_enabled",true);
        }
    }
}
