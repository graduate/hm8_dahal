/*
Gaurab Dahal (L20432055)
 */

package gaurabdahal.com.hm8_dahal;

import android.os.Build;
import android.preference.PreferenceActivity;

import java.util.List;

/**
 * Created by gaurabdahal on 7/11/17.
 */
public class PrefsActivity extends PreferenceActivity {

    @Override
    protected boolean isValidFragment(String fragmentName){
        if(Build.VERSION.SDK_INT< Build.VERSION_CODES.HONEYCOMB){
            return true;
        }else if(PrefsFragmentSettings.class.getName().equals(fragmentName)){
            return true;
        }
        return false;
    }

    public void onBuildHeaders(List<Header> target){
        getFragmentManager().beginTransaction().replace(android.R.id.content,new PrefsFragmentSettings()).commit();
    }
}
