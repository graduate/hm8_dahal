/*
Gaurab Dahal (L20432055)
 */
package gaurabdahal.com.hm8_dahal;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class SecondActivity extends AppCompatActivity implements View.OnClickListener {

    int images[] = {R.drawable.tiger,R.drawable.fox,R.drawable.lion,R.drawable.rhino,R.drawable.elephant};
    int sounds[] = {R.raw.sound_tiger,R.raw.sound_fox,R.raw.sound_lion,R.raw.sound_rhino, R.raw.sound_elephant};
    String soundNames[]={"Roar","Bark","Growl","Bellow","Trumpet"};
    int currentImage;
    private ImageView imageView;
    private Button prevButton, nextButton, infoButton;

    private SoundPool sp;

    int sound_click;
    int[] loaded_sound = new int[sounds.length];
    int num_sounds_loaded;
    boolean is_sound_loaded;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        imageView = (ImageView) findViewById(R.id.imageView);
        prevButton = (Button) findViewById(R.id.prevButton);
        nextButton = (Button) findViewById(R.id.nextButton);
        infoButton = (Button) findViewById(R.id.infoButton);



        if(savedInstanceState != null)
            currentImage = savedInstanceState.getInt("currentImage");
        else
            currentImage=0;

        nextButton.setOnClickListener(this);
        prevButton.setOnClickListener(this);
        imageView.setOnClickListener(this);
        infoButton.setOnClickListener(this);

        num_sounds_loaded=0;
        is_sound_loaded=false;

        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP){
            sp=new SoundPool(2, AudioManager.STREAM_MUSIC,0);
        }else{
            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();
            sp = new SoundPool.Builder()
                    .setAudioAttributes(attributes)
                    .build();
        }


        sp.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener(){
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                num_sounds_loaded++;
                if(num_sounds_loaded==sounds.length+1){
                    is_sound_loaded=true;
                }
            }
        });

        sound_click = sp.load(this,R.raw.sound_click,1);
        for(int i=0;i< sounds.length;i++){
            loaded_sound[i] = sp.load(this,sounds[i],1);
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.prevButton:
                if(currentImage == 0){
                    currentImage=images.length-1;
                }
                else if(currentImage >0 ){
                    currentImage--;
                }
                if(is_sound_loaded){
                    sp.play(sound_click,1,1,0,0,1);
                }
                imageView.setImageResource(images[currentImage]);
                break;
            case R.id.nextButton:
                if(currentImage == images.length-1){
                    currentImage = 0;
                }
                else if(currentImage < images.length-1){
                    currentImage++;
                }
                if(is_sound_loaded){
                    sp.play(sound_click,1,1,0,0,1);
                }
                imageView.setImageResource(images[currentImage]);
                break;
            case R.id.imageView:
                if(is_sound_loaded && Assets.soundpool_active){
                    sp.play(loaded_sound[currentImage],1,1,0,0,1);
                }else{
                    Toast.makeText(SecondActivity.this,soundNames[currentImage],Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.infoButton:
                Intent mIntent = new Intent(SecondActivity.this,PrefsActivity.class);
                startActivity(mIntent);
                break;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle b){
        b.putInt("currentImage", currentImage);
    }

    @Override
    public void onResume(){
        super.onResume();
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean musicEnabled = prefs.getBoolean("key_music_enabled", true);
        Assets.soundpool_active = prefs.getBoolean("key_soundeffect_enabled",true);
        imageView.setImageResource(images[currentImage]);
        if( Assets.mp != null){
            Assets.mp.start();
        }else{
            Assets.mp = MediaPlayer.create(this, R.raw.sound_forrest);
            Assets.mp.setLooping(true);
            Assets.mp.start();
            if(musicEnabled) {
                Assets.mp.setVolume(1,1);
            }else{
                Assets.mp.setVolume(0,0);
            }
        }
    }

    @Override
    public void onPause(){
        if(Assets.mp != null){
            Assets.mp.pause();
            if(isFinishing()) {
                Assets.mp.release();
                Assets.mp = null;
            }
        }
        super.onPause();
    }
    @Override
    public void onBackPressed(){
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Closing Application")
                .setMessage("Are you sure you want to exit?")
                .setPositiveButton("yes",new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which){
                            finish();
                    }
                })

                .setNegativeButton("No",null).show();
    }
}
