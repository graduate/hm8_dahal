/*
Gaurab Dahal (L20432055)
 */


package gaurabdahal.com.hm8_dahal;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.os.Handler;

public class MainActivity extends AppCompatActivity{

    private Handler mHandler = new Handler();
    boolean quit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        quit=false;
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(!quit){
                    startTheZoo();
                }
            }
        },2000);
    }

    private void startTheZoo(){
        Intent intent = new Intent(MainActivity.this,SecondActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed(){
        quit=true;
        super.onBackPressed();
    }
}
